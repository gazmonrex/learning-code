from bs4 import BeautifulSoup, NavigableString        # To parse the html data once retrieved
import requests                                       # To scrape and return the webpage
import pandas as pd                                   # To put into dataframes / load to tables?
import re                                             # re=regular expressions : to handle splitting of text in the html
    
url = "https://en.wikipedia.org/wiki/Sidney_Meyers"                
response = requests.get(url, timeout=5)                         # Make a request to download the page
content = BeautifulSoup(response.content, "html.parser")        # Pass the page into BeautifulSoup


############################ GET BIO ################################
bio = content.find('span', attrs={"id": "Biography"})                                  # The top tag that starts the bi section
bioend = content.find('span', attrs={"id": "Film_editing_in_the_pre-digital_era"})     # the start of the next section 

### function to get everything between the 2 tags as above - stolen from the internet!
def between(cur, end):
    while cur and cur != end:
        if isinstance(cur, NavigableString):
            text = cur.strip()
            if len(text):
                yield text
        cur = cur.next_element

biofilename=str(bio.text + '-SidneyMeyers.csv')        # create the filename we want the file to be called
file=open(biofilename, 'w', encoding='UTF-8')          # Create/open the file to write our results to          
file.write(' '.join(text + '\n' for text in between(bio.next_sibling,bioend)))     # write the result - n.b. no idea how this works!
                                                                                   # '.next_sibling' is the next 'line' of html

file.close                                             # Close the file
print(bio.text + '-SidneyMeyers.csv written to.')      # Send a message to the window to confirm what we've done
    
    
######################### GET FILM LIST ###############################
sectitle = content.find('span', attrs={"id": "Films_and_TV--partial_list"})        # Find the 'title' tag for the film list
filmList:list = sectitle.find_next('ul')         # As this is a nested list we can use the find_next to grab it - returns string

mylist:list = filmList.text.split('\n')          # Split the string into a list, on each new line
filename=str(sectitle.text + '.csv')             # create the filename we want the file to be called
file=open(filename, 'w')                         # Create/open the file to write our results to

### Now for each item in this list, use RegEx to split this into film name, and role
for f in mylist:
    title=re.split('--| - | – ',f)[0]                            # gets the film name (i.e. first word)
    roles=re.split('--| - | – ',f)[1]                            # gets the role (i.e. second word)
    file.write('"' + title + '","' + roles + '"')                # concatenate into a , delimeted string and write to the file
    file.write("\n")                                             # write a new line or at each iteration it just overwrites the one line

file.close()                                                     # Close the file
print(sectitle.text + '.csv written to.')                        # Send a message to the window to confirm what we've done


######################### GET AWARDS ###################################
awardstitle = content.find('span', attrs={"id": "Awards_and_nominations"})             # Find the 'title' tag for the award list
awardsfilename=(awardstitle.text + '.csv')       # create the filename we want the file to be called
file=open(awardsfilename, 'w')                   # Create/open the file to write our results to
for awards in awardstitle.find_all_next('p'):    # Awards are not in a list but each seperate paragraph, so use find_all_next 
                                                 # to get all the 'p' tags that follow this section
    doc = awards.find('i')                       # It's oddly ordered but we need to find the italics 'i' tag in each string
    year = doc.find_previous()                   # Then we step back to the previous bit of text for the 'year' part of the string
    tagend = str(awards).index("/i>") - 6        # then we need to find the position of the closing italics tag for the 'substring' == []

    file.write(year.text[0:4] + ',"' + doc.text + '","' + year.text[tagend:].strip() + '"')     # Write the data as ',' delim.
    file.write("\n")                             # Move to next line
file.close()                                     # Close the file
print(awardstitle.text + '.csv written to.')     # Send a message to the window to confirm what we've done