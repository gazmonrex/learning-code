def fizzbuzzGame(nums:list,subs:list,countlimit:int = 100):
    
#nums #Define your to-be-replaced numbers
#subs #Define your replacement values
#countlimit  #Set up how many numbers you want to play for - defaults to 100

################### ERROR HANDLING ###################
    ######### Numbers to sub must be actual numbers! We haven't got to subbing the subs yet! #########
    if [x for x in nums if not isinstance(x, int)]:
        print("Your substitutable numbers, must be numbers!")  
        return    

    ######### Handle the error if there are no numbers to be substituted #########
    if not nums:
        print("Have you understood how this game works?!")
        return
    
    ######### Handle the error if there are not enough counted numbers to reach the smallest sub number #########
    if not isinstance(countlimit,int) or countlimit < min(nums):
        print("You must specify a number, that is larger than the smallest substitutable number, to play this game")
        return
        
    ######### Handle the error if not enough substitution values have been given #########
    if len(subs)<len(nums): 
        print("You cannot play the game with more numbers than word substitutes!")
        return
     
################### PLAY THE GAME #####################    
    for i in range(1,countlimit+1):      #loop over each number up to your count limit
        
        myout=""                         #Initialise the output string to be blank at the start of each iteration
        
        for j in nums:     #Check and loop through each element in your to-be-replaced array
            
            if i%j == 0: myout += str(subs[nums.index(j)])     #Test each number, if it's divisible by one of the array values, 
                                                           #add that replacement string to your output string
                    
        if myout == "": myout = i        #Finally, if the string is still blank (i.e. not divisible by any of the factors) 
                                         #write the original number to the output string
                
        print(myout)   #print whatever in now in the output string
    
fizzbuzzGame([3,5,10,12],["fizz","buzz","wuzz","cuzz"])